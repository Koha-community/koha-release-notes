# RELEASE NOTES FOR KOHA 20.11.02
09 Feb 2021

Koha is the first free and open source software library automation
package (ILS). Development is sponsored by libraries of varying types
and sizes, volunteers, and support companies from around the world. The
website for the Koha project is:

- [Koha Community](http://koha-community.org)

Koha 20.11.02 can be downloaded from:

- [Download](http://download.koha-community.org/koha-20.11.02.tar.gz)

Installation instructions can be found at:

- [Koha Wiki](http://wiki.koha-community.org/wiki/Installation_Documentation)
- OR in the INSTALL files that come in the tarball

Koha 20.11.02 is a bugfix/maintenance release.

It includes 2 new features, 13 enhancements, 46 bugfixes.

### System requirements

Koha is continuously tested against the following configurations and as such these are the recommendations for 
deployment: 

- Debian Jessie with MySQL 5.5 (End of life)
- Debian Stretch with MariaDB 10.1
- Debian Buster with MariaDB 10.3
- Ubuntu Bionic with MariaDB 10.1 
- Debian Stretch with MySQL 8.0 (Experimental MySQL 8.0 support)

Additional notes:
    
- Perl 5.10 is required (5.24 is recommended)
- Zebra or Elasticsearch is required



## New features

### Plugin architecture

- [[25245]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25245) Add a plugin hook to allow running code on a nightly basis

  >This patchset adds a new cronjob script to Koha, plugins_nightly.pl
  >
  >This script will check for plugins that have registered a cronjob_nightly method and execute that method.
  >
  >This enhancement allows users to install and setup plugins that require cronjobs without backend system changes and prevents the addition of new cronjob files for each plugin.

### Staff Client

- [[14004]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=14004) Add ability to temporarily disable added CSS and Javascript in OPAC and interface

  >This allows to temporarily disable any of OPACUserCSS, OPACUserJS, OpacAdditionalStylesheet, opaclayoutstylesheet, IntranetUserCSS, IntranetUserJS, intranetcolorstylesheet, and intranetstylesheet system preference via an URL parameter.
  >
  >Alter the URL in OPAC or staff interface by adding an additional parameter DISABLE_SYSPREF_<system preference name>=1. 
  >
  >Example:
  >/cgi-bin/koha/mainpage.pl?DISABLE_SYSPREF_IntranetUserCSS=1

## Enhancements

### Acquisitions

- [[27023]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27023) Add class names in the suggestions column in suggestions management

### Command-line Utilities

- [[24272]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24272) Add a command line script to compare the syspref cache to the database

  >This script checks the value of the system preferences in the database against those in the cache. Generally differences will only exist if changes have been made directly to the DB or the cache has become corrupted.

### MARC Bibliographic data support

- [[27022]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27022) Add publisher number (MARC21 028) to OPAC and staff detail pages

### Notices

- [[11257]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=11257) Document <<items.content>> for DUEDGST and PREDUEDGST and update sample notices

### OPAC

- [[27029]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27029) Detail page missing Javascript accessible biblionumber value
- [[27098]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27098) Rename 'Relatives fines' to 'Relatives charges' in OPAC

### Reports

- [[22152]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=22152) Hide printing the tools navigation when printing reports

### Searching - Elasticsearch

- [[26991]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26991) Add action logs to search engine administration

  >This enhancement adds logging of changes made to Elasticsearch. These can be viewed in the log viewer tool, where you can view all search engine changes, or limit to edit mapping and reset mapping actions.

### System Administration

- [[27395]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27395) Add warning to PatronSelfRegistrationDefaultCategory to avoid accidental patron deletion

  >This patch adds a warning to the PatronSelfRegistrationDefaultCategory system
  >preference to not use a regular patron category for self registration.
  >
  >If a regular patron category code is used and the cleanup_database cronjob is setup
  >to delete unverified and unfinished OPAC self registrations, it permanently and
  >and unrecoverably deletes all patrons that have registered more than
  >PatronSelfRegistrationExpireTemporaryAccountsDelay days ago.
  >
  >It also removes unnecessary apostrophes at the end of two self registration
  >and modification system preference descriptions.

### Templates

- [[26755]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26755) Make the guarantor search popup taller
- [[26982]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26982) Typo in system preference UsageStats: statisics
- [[27192]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27192) Set focus for cursor to item type input box when creating new item types
- [[27210]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27210) Typo in patron-attr-types.tt


## Critical bugs fixed

### Architecture, internals, and plumbing

- [[27580]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27580) NULL values not correctly handled in Koha::Items->filter_by_visible_in_opac


## Other bugs fixed

### About

- [[7143]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=7143) Bug for tracking changes to the about page

### Acquisitions

- [[27446]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27446) Markup errors in suggestion/suggestion.tt

  >This patch fixes several markup errors in the suggestions template in the staff interface, including:
  >- Indentation
  >- Unclosed tags
  >- Non-unique IDs
  >- Adding comments to highlight markup structure
- [[27547]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27547) Typo in parcel.tt

### Architecture, internals, and plumbing

- [[25381]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25381) XSLTs should not define entities
- [[25552]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25552) Add missing Claims Returned option to MarkLostItemsAsReturned

  >Marking an item as a return claim checks the system preference MarkLostItemsAsReturned to see if the claim should be removed from the patron record. However, the option for "when marking an item as a return claim" was never added to the system preference, so there was no way to remove a checkout from the patron record when marking the checkout as a return claim. This patch set adds that missing option.
- [[27179]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27179) Misspelling of Method in REST API files

  >This fixes the misspelling of Method (Mehtod to Method) in REST API files.
- [[27327]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27327) Indirect object notation in Koha::Club::Hold
- [[27333]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27333) Wrong exception thrown in Koha::Club::Hold::add
- [[27530]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27530) Sample patron data should be updated and/or use relative dates

### Cataloging

- [[27508]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27508) Can't duplicate the MARC field tag with JavaScript if option "advancedMARCeditor" is set to "Don't display"

### Circulation

- [[27011]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27011) Warnings in returns.pl

  >This patch removes a variable ($name) that is no longer used in Circulation > Check in (/cgi-bin/koha/circ/returns.pl), and the resulting warnings (..[WARN] Use of uninitialized value in concatenation (.) or string at..) that were recorded in the error log.
- [[27538]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27538) Cells in the bottom filtering row of the "Holds to pull" table shifted
- [[27548]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27548) Warnings "use of uninitialized value" on branchoverdues.pl

  >This fixes the cause of unnecessary "use of uninitialized value" warnings in the log files generated by Circulation > Overdues with fines (/cgi-bin/koha/circ/branchoverdues.pl).
  >
  >This was caused by not taking into account that the "location" parameter for this form is initially empty.
- [[27549]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27549) Warning "use of uninitialized value" on renew.pl

  >This fixes the cause of unnecessary "use of uninitialized value" warnings in the log files generated by Circulation > Renew (/cgi-bin/koha/circ/renew.pl).
  >
  >This was caused by not taking into account that the "barcode" parameter for this form is initially empty.

### Command-line Utilities

- [[11344]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=11344) Perldoc issues in misc/cronjobs/advance_notices.pl

### Fines and fees

- [[20527]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20527) <label> linked to the wrong <input> (wrong "for" attribute) in paycollect.tt

  >This fixes the HTML <label for=""> element for the Writeoff amount field on the Accounting > Make a payment form for a patron - changes "paid" to "amountwrittenoff".

### I18N/L10N

- [[27416]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27416) String 'Modify tag' in breadcrumb is untranslatable

### ILL

- [[25614]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25614) "Clear filter" button permanently disabled on ILL request list

### Installation and upgrade (web-based installer)

- [[11996]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=11996) Default active currencies for ru-RU and uk-UA are wrong

  >This fixes the currencies in the sample installer files for Russia (ru-RU; changes GRN -> UAH, default remains as RUB) and the Ukraine (uk-UA; changes GRN -> UAH).
- [[24810]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24810) French SQL files for "news" contain "Welcome into Koha 3!"

  >This removes the Koha version number from the sample news items for the French language installer files (fr-FR and fr-CA).
- [[24811]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24811) French SQL files for "news" contain broken link to the wiki

  >This fixes a broken link in the sample news items for the French language installer files (fr-FR and fr-CA).

### Notices

- [[24447]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24447) POD of C4::Members::Messaging::GetMessagingPreferences() is misleading

### OPAC

- [[26578]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26578) OverDrive results can return false positives when searches contain CCL syntax
- [[27261]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27261) PatronSelfRegistrationBorrowerUnwantedField should exclude branchcode

  >This patch excludes the ability to add branchcode to the PatronSelfRegistrationBorrowerUnwantedField system preference.
- [[27450]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27450) Making password required for patron registration breaks patron modification
- [[27543]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27543) Tooltip on opac-messaging.pl obscured by table headers

  >This patch fixes the display of tooltips in a patrons OPAC account for the 'your messaging' section. It corrects which Bootstrap assets are compiled with the OPAC CSS - the file for Bootstrap tooltips should be included.
- [[27571]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27571) "Add to lists" on MARC and ISBD view of OPAC detail page doesn't open in new window

### Patrons

- [[17364]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17364) branchcode in BorrowerUnwantedField causes software error when saving patron record

  >A more user friendly interface for selecting database columns for some system preferences (such as BorrowerUnwantedField) was added in Koha 20.11 (Bug 22844). 
  >
  >Some database columns should be excluded from selection as they can cause server errors. For example, branchcode in BorrowerUnwantedField is required for adding patrons - if selected it causes a server error and you can't add a patron, so it should not be selectable.
  >
  >This big fixes the issue by:
  >
  >- allowing developers to define the database columns to exclude from selection in the .pref system preference definition file using "exclusions: "
  >
  >- disabling the selection of the excluded database columns in the staff interface when configuring system preferences that allow selecting database columns
  >
  >- updating the BorrowerUnwantedField system preference to exclude branchcode

### SIP2

- [[25808]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=25808) Renewal via the SIP 'checkout' message gives incorrect message
- [[27204]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27204) SIP patron information request with fee line items returns incorrect data

### Staff Client

- [[27321]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27321) Make excluded database columns in system preferences more clearly disabled

  >This enhancement styles non-selectable database columns in system preferences in a light grey (#cccccc), making them easier to identify. Currently the checkbox and label are the same color as selectable columns.

### System Administration

- [[27264]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27264) Reword sentence of OPACHoldsHistory

### Templates

- [[20238]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=20238) Show description of ITEMTYPECAT instead of code in itemtypes summary table

  >This enhancement changes the item types page (Koha administration > Basic parameters > Item types) so that the search category column displays the ITEMTYPECAT authorized value's description, instead of the authorized value code. (This makes it consistent with the edit form where it displays the descriptions for authorized values.)
- [[24055]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=24055) Description of PayPalReturnURL system preference is unclear

  >This enhancement improves the description of the PayPalReturnURL. Changed from 'configured return' to 'configured return URL' as this is what it is called on the PayPal website.
- [[27027]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27027) Typo: has successfully been modified.. %s

  >This fixes a grammatical error in koha-tmpl/intranet-tmpl/prog/en/modules/admin/background_jobs.tt (has successfully been modified..) - it replaces two full stops at the end of the sentence with one.
- [[27430]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27430) Use minimum length for patron category on password hint

  >This corrects the hint on the patron add/edit form to take into account that the minimum password length can now also be set on patron category level.
- [[27457]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27457) Set focus for cursor to Debit type code field when creating new debit type
- [[27458]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27458) Set focus for cursor to Credit type code field when creating new credit type
- [[27525]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27525) 'wich' instead of a 'with' in a sentence

  >This patch fixes two spelling errors in the batchMod-del.tt template that is used by the batch item deletion tool in the staff interface: "wich" -> "with."
- [[27531]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27531) Remove type attribute from script tags: Cataloging plugins
- [[27561]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27561) Remove type attribute from script tags: Various templates

### Test Suite

- [[27554]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27554) Clarify and add tests for Koha::Patrons->update_category_to child to adult

### Tools

- [[26298]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=26298) If MaxItemsToProcessForBatchMod is set to 1000, the max is actually 999
- [[27576]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=27576) Don't show import records table when cleaning a batch

### Web services

- [[17229]](http://bugs.koha-community.org/bugzilla3/show_bug.cgi?id=17229) ILS-DI HoldTitle and HoldItem should check if patron is expired


## Documentation

The Koha manual is maintained in Sphinx. The home page for Koha 
documentation is 

- [Koha Documentation](http://koha-community.org/documentation/)

As of the date of these release notes, only the English version of the
Koha manual is available:

- [Koha Manual](http://koha-community.org/manual/20.11/en/html/)


The Git repository for the Koha manual can be found at

- [Koha Git Repository](https://gitlab.com/koha-community/koha-manual)

## Translations

Complete or near-complete translations of the OPAC and staff
interface are available in this release for the following languages:

- Arabic (90.6%)
- Armenian (100%)
- Armenian (Classical) (89%)
- Chinese (Taiwan) (87%)
- Czech (73.4%)
- English (New Zealand) (59.8%)
- English (USA)
- Finnish (78.5%)
- French (75.7%)
- French (Canada) (91.5%)
- German (100%)
- German (Switzerland) (67.2%)
- Greek (60.7%)
- Hindi (95.4%)
- Italian (100%)
- Norwegian Bokmål (63.7%)
- Polish (71.2%)
- Portuguese (77.6%)
- Portuguese (Brazil) (89.1%)
- Russian (89.4%)
- Slovak (81%)
- Spanish (94.4%)
- Swedish (74.9%)
- Telugu (80%)
- Turkish (89.2%)
- Ukrainian (63.1%)

Partial translations are available for various other languages.

The Koha team welcomes additional translations; please see

- [Koha Translation Info](http://wiki.koha-community.org/wiki/Translating_Koha)

For information about translating Koha, and join the koha-translate 
list to volunteer:

- [Koha Translate List](http://lists.koha-community.org/cgi-bin/mailman/listinfo/koha-translate)

The most up-to-date translations can be found at:

- [Koha Translation](http://translate.koha-community.org/)

## Release Team

The release team for Koha 20.11.02 is


- Release Manager: Jonathan Druart

- Release Manager assistants:
  - Martin Renvoize
  - Tomás Cohen Arazi

- QA Manager: Katrin Fischer

- QA Team:
  - Marcel de Rooy
  - Joonas Kylmälä
  - Josef Moravec
  - Tomás Cohen Arazi
  - Nick Clemens
  - Kyle Hall
  - Martin Renvoize
  - Alex Arnaud
  - Julian Maurice
  - Matthias Meusburger

- Topic Experts:
  - Elasticsearch -- Frédéric Demians
  - REST API -- Tomás Cohen Arazi
  - UI Design -- Owen Leonard
  - Zebra -- Fridolin Somers
  - Accounts -- Martin Renvoize
  - CAS/Shibboleth -- Matthias Meusburger

- Bug Wranglers:
  - Michal Denár
  - Holly Cooper
  - Henry Bolshaw
  - Lisette Scheer
  - Mengü Yazıcıoğlu

- Packaging Manager: Mason James


- Documentation Managers:
  - Caroline Cyr La Rose
  - David Nind

- Documentation Team:
  - Martin Renvoize
  - Donna Bachowski
  - Lucy Vaux-Harvey
  - Kelly McElligott
  - Jessica Zairo
  - Chris Cormack
  - Henry Bolshaw
  - Jon Drucker

- Translation Manager: Bernardo González Kriegel


- Release Maintainers:
  - 20.05 -- Lucas Gass
  - 19.11 -- Aleisha Amohia
  - 19.05 -- Victor Grousset

- Release Maintainer mentors:
  - 19.11 -- Hayley Mapley
  - 19.05 -- Martin Renvoize

## Credits

We thank the following individuals who contributed patches to Koha 20.11.02.

- Ethan Amohia (1)
- Tomás Cohen Arazi (5)
- Eden Bacani (5)
- Nick Clemens (8)
- David Cook (1)
- Jonathan Druart (13)
- Katrin Fischer (5)
- Lucas Gass (3)
- Didier Gautheron (1)
- Kyle M Hall (7)
- Mazen Khallaf (3)
- Amy King (4)
- Owen Leonard (8)
- ava li (2)
- Catherine Ma (2)
- Julian Maurice (2)
- James O'Keeffe (3)
- Martin Renvoize (6)
- Marcel de Rooy (2)
- Caroline Cyr La Rose (2)
- Andreas Roussos (1)
- Fridolin Somers (7)
- Arthur Suzuki (1)
- Emmi Takkinen (2)
- Petro Vashchuk (4)
- Ella Wipatene (2)

We thank the following libraries, companies, and other institutions who contributed
patches to Koha 20.11.02

- Athens County Public Libraries (8)
- BibLibre (11)
- BSZ BW (5)
- ByWater-Solutions (18)
- Dataly Tech (1)
- gamil.com (3)
- Independant Individuals (23)
- Koha Community Developers (13)
- Koha-Suomi (2)
- Prosentient Systems (1)
- PTFS-Europe (6)
- Rijks Museum (2)
- Solutions inLibro inc (2)
- Theke Solutions (5)

We also especially thank the following individuals who tested patches
for Koha.

- Mark Hofstetter <mark@hofstetter.at>, (1)
- Tomás Cohen Arazi (3)
- Eden Bacani (1)
- Nick Clemens (13)
- Michal Denar (1)
- Jonathan Druart (85)
- Katrin Fischer (45)
- Andrew Fuerste-Henry (3)
- Marti Fyerst (3)
- Brendan Gallagher (2)
- Lucas Gass (3)
- Victor Grousset (1)
- Kyle M Hall (8)
- Sally Healey (6)
- Barbara Johnson (5)
- Joonas Kylmälä (1)
- Owen Leonard (8)
- Julian Maurice (1)
- Kelly McElligott (2)
- David Nind (19)
- Martin Renvoize (19)
- Marcel de Rooy (8)
- Fridolin Somers (98)



We regret any omissions.  If a contributor has been inadvertently missed,
please send a patch against these release notes to 
koha-patches@lists.koha-community.org.

## Revision control notes

The Koha project uses Git for version control.  The current development 
version of Koha can be retrieved by checking out the master branch of:

- [Koha Git Repository](git://git.koha-community.org/koha.git)

The branch for this version of Koha and future bugfixes in this release
line is 20.11.x.

## Bugs and feature requests

Bug reports and feature requests can be filed at the Koha bug
tracker at:

- [Koha Bugzilla](http://bugs.koha-community.org)

He rau ringa e oti ai.
(Many hands finish the work)

Autogenerated release notes updated last on 09 Feb 2021 11:46:06.
